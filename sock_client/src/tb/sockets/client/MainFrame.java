package tb.sockets.client;

import javax.swing.JFrame;

public class MainFrame {
	public static void main(String[] args) throws Exception {
        while (true) {
            String serverAddress = "localhost";
            Konsola kikClient = new Konsola(serverAddress);
            kikClient.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            kikClient.frame.setSize(400, 400);
            kikClient.frame.setVisible(true);
            kikClient.play();
            if (!kikClient.playAgain()) {
                break;
            }
        }
    }
}
