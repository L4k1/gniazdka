package tb.sockets.client;

import java.net.Socket;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Konsola {

	private static int PORT = 6666;
	private Socket socket;
	private BufferedReader bufIn;
	private PrintWriter printOut;
	
    JFrame frame = new JFrame("Kolko i krzyzyk");
    private JLabel message = new JLabel();
    private ImageIcon imageIcon;
    private ImageIcon enemyImageIcon;
  
    public Konsola(String servAdress) throws Exception {

        socket = new Socket(servAdress, PORT);
        bufIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        printOut = new PrintWriter(socket.getOutputStream(), true);
        message.setBackground(Color.WHITE);
        frame.getContentPane().add(message, "North");
        
        JPanel fieldPanel = new JPanel();
        fieldPanel.setBackground(Color.GREEN);
        fieldPanel.setLayout(new GridLayout(3, 3, 2, 2));
        for (int i = 0; i < field.length; i++) {
        	int j = i;
            field[i] = new Box();
            field[i].addMouseListener(new MouseAdapter() {
                public void mousePressed(MouseEvent e) {
                    actualBox = field[j];
                    printOut.println("MOVE " + j);
                    }
                });
            fieldPanel.add(field[i]);
        }
        frame.getContentPane().add(fieldPanel, "Center");
    }
   
    public static class Box extends JPanel {
        JLabel label = new JLabel((Icon)null);
        public Box() {
            setBackground(Color.white);
            add(label);
        }
        public void setIcon(Icon icon) {
            label.setIcon(icon);
        }
    }
    
    private Box[] field = new Box[9];
    private Box actualBox;
    
    public void play() throws Exception {
        String reply;
        try {
            reply = bufIn.readLine();
            if (reply.startsWith("WELCOME")) {
                char sign = reply.charAt(8);
                imageIcon = new ImageIcon(sign == 'X' ? "C:\\Users\\�ukasz\\git\\gniazdka\\sock_client\\resources\\X.png" : "C:\\Users\\�ukasz\\git\\gniazdka\\sock_client\\resources\\O.png");
                enemyImageIcon  = new ImageIcon(sign == 'X' ? "C:\\Users\\�ukasz\\git\\gniazdka\\sock_client\\resources\\O.png" : "C:\\Users\\�ukasz\\git\\gniazdka\\sock_client\\resources\\X.png");
            }
            while (true) {
                reply = bufIn.readLine();
                if (reply.startsWith("VALID_MOVE")) {
                    actualBox.setIcon(imageIcon);
                    actualBox.repaint();
                } else if (reply.startsWith("OPPONENT_MOVED")) {
                    int lok = Integer.parseInt(reply.substring(15));
                    field[lok].setIcon(enemyImageIcon);
                    field[lok].repaint();
                } else if (reply.startsWith("VICTORY")) {
                    message.setText("Wygra�e�");
                    break;
                } else if (reply.startsWith("DEFEAT")) {
                    message.setText("Przegra�e�");
                    break;
                } else if (reply.startsWith("TIE")) {
                    message.setText("Remis");
                    break;
                } else if (reply.startsWith("MESSAGE")) {
                    message.setText(reply.substring(8));
                }
            }
           printOut.println("QUIT");
        }
        finally {
            socket.close();
        }
    }

    boolean playAgain() {
        int reply = JOptionPane.showConfirmDialog(frame, "Czy chcesz zagra� jeszcze raz?", "", JOptionPane.YES_NO_OPTION);
        frame.dispose();
        return reply == JOptionPane.YES_OPTION;
    }
}

