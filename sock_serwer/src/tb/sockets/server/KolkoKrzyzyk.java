package tb.sockets.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class KolkoKrzyzyk {
	
	class Gamer extends Thread {
        char sign;
        Gamer enemy;
        Socket socket;
        BufferedReader input;
        PrintWriter output;
 
        public Gamer(Socket socket, char sign) {
            this.socket = socket;
            this.sign = sign;
            try {
                input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                output = new PrintWriter(socket.getOutputStream(), true);
                output.println("WELCOME " + sign);
            } catch (IOException e) {
            	System.out.println(e);
            }
        }
        
        public void setOpponent(Gamer enemy) {
            this.enemy = enemy;
        }

        public void otherPlayerMoved(int localization) {
            output.println("OPPONENT_MOVED " + localization);
            output.println(
                isWiner() ? "DEFEAT" : isFilledUp() ? "TIE" : "");
        }
    
        public void run() {
            try {
            	output.println("MESSAGE Gracze po��czeni");
                if (sign == 'X') {
                	output.println("MESSAGE Tw�j ruch");
                }
                while (true) {
                    String command = input.readLine();
                    if (command.startsWith("MOVE")) {
                        int localization = Integer.parseInt(command.substring(5));
                        if (Move(localization, this)) {
                            output.println("VALID_MOVE");
                            output.println(isWiner() ? "VICTORY": isFilledUp() ? "TIE" : "");
                        } else {
                            output.println("MESSAGE  ");
                        }
                    } else if (command.startsWith("QUIT")) {
                        return;
                    }
                }
            } catch (IOException e) {
            } finally {
                try {
                	socket.close();
                	} catch (IOException e) {
                		System.out.println(e);
                	}
            }
        }
    }
	
    Gamer actualGamer;
    
    private Gamer[] field = {
        null, null, null,
        null, null, null,
        null, null, null};

    public boolean isWiner() {
        return
            (field[0] != null && field[0] == field[1] && field[0] == field[2])
          ||(field[3] != null && field[3] == field[4] && field[3] == field[5])
          ||(field[6] != null && field[6] == field[7] && field[6] == field[8])
          ||(field[0] != null && field[0] == field[3] && field[0] == field[6])
          ||(field[1] != null && field[1] == field[4] && field[1] == field[7])
          ||(field[2] != null && field[2] == field[5] && field[2] == field[8])
          ||(field[0] != null && field[0] == field[4] && field[0] == field[8])
          ||(field[2] != null && field[2] == field[4] && field[2] == field[6]);
    }

    public boolean isFilledUp() {
        for (int i = 0; i < field.length; i++) {
            if (field[i] == null) {
                return false;
            }
        }
        return true;
    }

    public boolean Move(int location, Gamer player) {
        if (player == actualGamer && field[location] == null) {
            field[location] = actualGamer;
            actualGamer = actualGamer.enemy;
            actualGamer.otherPlayerMoved(location);
            return true;
        }
        return false;
    }
}
