package tb.sockets.server;

import java.net.ServerSocket;

public class Konsola {

    public static void main(String[] args) throws Exception {
        ServerSocket listener = new ServerSocket(6666);
        System.out.println("Serwer dzia�a");
        try {
            while (true) {
                KolkoKrzyzyk kik = new KolkoKrzyzyk();
                KolkoKrzyzyk.Gamer player1 = kik.new Gamer(listener.accept(), 'X');
                KolkoKrzyzyk.Gamer player2 = kik.new Gamer(listener.accept(), 'O');
                player1.setOpponent(player2);
                player2.setOpponent(player1);
                kik.actualGamer = player1;
                player1.start();
                player2.start();
            }
        } finally {
            listener.close();
        }
    }
}
